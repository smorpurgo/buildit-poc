package org.morpurgo.buildit.crawler.spider;

import org.junit.Test;
import org.morpurgo.buildit.crawler.spider.resource.CrawledResource;
import org.morpurgo.buildit.crawler.spider.resource.ImageResource;
import org.morpurgo.buildit.crawler.spider.resource.WebPage;

import java.net.URI;
import java.net.URISyntaxException;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Created by morpurgo on 14/09/16.
 */
public class ResourceNodeTest {


    @Test
    public void onlyRoot() throws URISyntaxException {
        CrawledResource rootData    =   new WebPage(new URI("http://prova.it"), null, null, null, null);
        ResourceNode root = new ResourceNode(rootData);
        assertNotNull("root", root);
        assertEquals("Same data", root.getData(), rootData);
        assertEquals("No children", 0, root.getChildren().size());
    }

    @Test
    public void addChild() throws URISyntaxException {
        CrawledResource rootData    =   new WebPage(new URI("http://prova.it"), null, null, null, null);
        ResourceNode root = new ResourceNode(rootData);
        root.addChild(new WebPage(
                new URI("childUri"), null, null, null, rootData.getResourceURI()
        ));
        assertEquals("1 children ", 1, root.getChildren().size());
    }

    @Test
    public void childOfChild() throws URISyntaxException {
        CrawledResource rootData    =   new WebPage(new URI("http://prova.it"),
                null, null, null, null);
        ResourceNode<CrawledResource> root = new ResourceNode(rootData);
        root.addChild(new WebPage(
                new URI("childUri"), null, null, null, rootData.getResourceURI()
        ));
        CrawledResource grandChildData
                =   new ImageResource("image.jpg", "childUri");
        ResourceNode childOfChild   =   new ResourceNode(grandChildData);
        root.addNodeByParentUrl(childOfChild);
        assertEquals("1 children ", 1, root.getChildren().size());
        assertEquals("1 children", 1, ((ResourceNode) root.getChildren().get(0)).getChildren().size());
    }


}