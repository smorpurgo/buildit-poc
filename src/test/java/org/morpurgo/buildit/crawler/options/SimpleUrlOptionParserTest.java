package org.morpurgo.buildit.crawler.options;

import org.junit.Test;
import org.morpurgo.buildit.crawler.exception.InvalidUrlException;

/**
 * Created by morpurgo on 13/09/16.
 */
public class SimpleUrlOptionParserTest {

    @Test(expected = InvalidUrlException.class)
    public void errorOnNotValidUrl() throws InvalidUrlException {
        SimpleUrlOptionParser parser = new SimpleUrlOptionParser();
        parser.parse(new String[] {"I'm not valid.com"});
    }

    @Test(expected = IllegalArgumentException.class)
    public void errorOnMissingUrl() throws InvalidUrlException {
        SimpleUrlOptionParser parser = new SimpleUrlOptionParser();
        parser.parse(new String[] {});
    }

}