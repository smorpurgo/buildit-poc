package org.morpurgo.buildit.crawler.options;

import org.morpurgo.buildit.crawler.exception.InvalidUrlException;

/**
 * Created by morpurgo on 13/09/16.
 */
public interface OptionParser {
    SpiderOptions parse(String[] args) throws InvalidUrlException;
}
