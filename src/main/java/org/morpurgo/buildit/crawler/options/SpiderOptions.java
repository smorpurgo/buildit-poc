package org.morpurgo.buildit.crawler.options;

import org.apache.commons.validator.routines.UrlValidator;
import org.morpurgo.buildit.crawler.exception.InvalidUrlException;

/**
 * Created by morpurgo on 13/09/16.
 */
public class SpiderOptions {

    private static final int DEFAULT_MAX_URLS = 1024;
    private final String urlToCrawl;
    private int maxUrls;

    public SpiderOptions(String urlToCrawl) throws InvalidUrlException {
        UrlValidator urlValidator = new UrlValidator();
        if (!urlValidator.isValid(urlToCrawl)) {
            throw new InvalidUrlException("Invalid URL ( " + urlToCrawl + ") url to crawl");
        }
        this.urlToCrawl = urlToCrawl;
        maxUrls = DEFAULT_MAX_URLS;
    }

    public String getUrlToCrawl() {
        return urlToCrawl;
    }

    public int getMaxUrls() {
        return maxUrls;
    }
}
