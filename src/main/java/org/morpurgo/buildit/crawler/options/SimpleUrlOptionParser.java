package org.morpurgo.buildit.crawler.options;

import org.morpurgo.buildit.crawler.exception.InvalidUrlException;
import org.springframework.stereotype.Component;

/**
 * Created by morpurgo on 13/09/16.
 */
@Component
public class SimpleUrlOptionParser implements OptionParser {

    public SpiderOptions parse(String[] args) throws InvalidUrlException {
        if (args == null || args.length < 1) {
            throw new IllegalArgumentException("Missing parameter." +
                    "params: 0 <url>");
        }


        if (org.apache.commons.lang.StringUtils.isBlank(args[0])) {
            throw new InvalidUrlException("Invalid url " + args[0] + " to crawl ( empty or null )");
        }
        return new SpiderOptions(args[0]);
    }

}
