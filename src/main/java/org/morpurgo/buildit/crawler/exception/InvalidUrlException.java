package org.morpurgo.buildit.crawler.exception;

/**
 * Created by morpurgo on 13/09/16.
 */
public class InvalidUrlException extends Exception {
    public InvalidUrlException(String message) {
        super(message);
    }
}
