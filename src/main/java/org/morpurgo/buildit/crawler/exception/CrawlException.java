package org.morpurgo.buildit.crawler.exception;

/**
 * Created by morpurgo on 13/09/16.
 */
public class CrawlException extends Exception {
    public CrawlException(String message, Throwable t) {
        super(message, t);
    }
}
