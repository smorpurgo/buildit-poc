package org.morpurgo.buildit.crawler.spider.resource;

import java.net.URI;

/**
 * Created by morpurgo on 13/09/16.
 */
public interface CrawledResource {

    URI getResourceURI();
    void setResourceURI(URI resourceURI);

    URI getParentURI();
}
