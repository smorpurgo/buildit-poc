package org.morpurgo.buildit.crawler.spider;

import org.morpurgo.buildit.crawler.spider.resource.CrawledResource;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by morpurgo on 14/09/16.
 */
public class ResourceNode<T extends CrawledResource> {

    private List<ResourceNode<T>> children = new ArrayList<>();
    private ResourceNode<T> parent = null;
    private T data = null;

    public ResourceNode(T data) {
        this.data = data;
    }

    public ResourceNode(T data, ResourceNode<T> parent) {
        this.data = data;
        this.setParent(parent);
    }

    public List<ResourceNode<T>> getChildren() {
        return children;
    }

    public void setParent(ResourceNode<T> parent) {
        parent.addChild(this);
        this.parent = parent;
    }

    public void addChild(T data) {
        ResourceNode<T> child = new ResourceNode<>(data);
        addChild(child);
    }

    public void addChild(ResourceNode<T> child) {
        child.parent = this;
        this.children.add(child);
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public boolean isRoot() {
        return (this.parent == null);
    }

    public boolean isLeaf() {
        if(this.children.size() == 0)
            return true;
        else
            return false;
    }

    public void removeParent() {
        this.parent = null;
    }

    public void addNodeByParentUrl(ResourceNode<T> resourceNode) {
        //find parent
        ResourceNode<T> parent = findNode(resourceNode.getData().getParentURI());
        if (parent != null) {
            parent.addChild(resourceNode);
        }
    }

    private ResourceNode<T> findNode(URI parentURI) {
        ResourceNode<T> found = null;
        if (this.getData().getResourceURI().equals(parentURI)) {
            found = this;
        } else {
            boolean foundNode   =   false;
            Iterator childrenIterator   =   this.children.iterator();
            while (!foundNode && childrenIterator.hasNext()) {
                ResourceNode<T> child = (ResourceNode<T>) childrenIterator.next();
                found = child.findNode(parentURI);
                if (found != null) {
                    foundNode = true;
                }
            }
        }
        return found;
    }
}