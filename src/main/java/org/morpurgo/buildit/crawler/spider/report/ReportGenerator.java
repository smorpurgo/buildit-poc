package org.morpurgo.buildit.crawler.spider.report;

import org.morpurgo.buildit.crawler.exception.CrawlException;
import org.morpurgo.buildit.crawler.spider.ResourceNode;
import org.morpurgo.buildit.crawler.spider.resource.WebPage;
import org.rendersnake.HtmlCanvas;
import org.springframework.stereotype.Component;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/**
 * Created by morpurgo on 13/09/16.
 */
@Component
public class ReportGenerator {

    private static final String DEFAULT_OUTPUT = "siteMap.html";

    /**
     * generate from crawled web pages an html report
     * it's a pure programmatically, while another approach could be using
     * templating libraries ( freemarker, or similar)
     *
     * @param resources
     */
    public String generateHtmlReport(ResourceNode<WebPage> resources) {
        HtmlCanvas html = new HtmlCanvas();
        try {
            final HtmlCanvas body = html.html().body();

            //do the work here
            WebPage resource = resources.getData();
            addResourceToElement(resource, resources.getChildren(), body);
            body._body()._html();

        } catch (IOException e) {
            throw new RuntimeException("Unable to generate report", e);
        }
        return html.toHtml();
    }

    private void addResourceToElement(WebPage resource,
                                      List<ResourceNode<WebPage>> children,
                                      HtmlCanvas element) throws IOException {
        HtmlCanvas ul = element.ul().li().content(resource.getResourceURI().toASCIIString());
        for (ResourceNode<WebPage> child : children) {
           addResourceToElement(child.getData(), child.getChildren(), ul);
        }
        element._ul();
    }

    public void writeToFile(String htmlReport) throws CrawlException {
        File file = new File(DEFAULT_OUTPUT);
        BufferedWriter writer = null;
        try {
            if (file.createNewFile()) {
                writer = new BufferedWriter(new FileWriter(file));
                writer.write(htmlReport);
            }

        } catch (IOException e) {
            throw new CrawlException("Unable to write html report", e);
        } finally {
            if (writer != null) {
                try {
                    writer.flush();
                    writer.close();
                } catch (IOException e) {
                    throw new CrawlException("Error while flushing data to file", e);
                }
            }
        }
    }
}
