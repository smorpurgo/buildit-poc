package org.morpurgo.buildit.crawler.spider.resource;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by morpurgo on 13/09/16.
 */
public abstract class AbstractCrawledResource implements CrawledResource {

    private URI resourceURI;
    private URI parentURI;

    public AbstractCrawledResource(URI uri, URI parentURI) {
        this.resourceURI = uri;
        this.parentURI = parentURI;
    }

    public AbstractCrawledResource(String url, String parentUrl) throws URISyntaxException {
        this(new URI(url), new URI(parentUrl));
    }

    @Override
    public URI getResourceURI() {
        return resourceURI;
    }

    @Override
    public void setResourceURI(URI resourceURI) {
        this.resourceURI = resourceURI;
    }

    @Override
    public URI getParentURI() {
        return parentURI;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof AbstractCrawledResource)) return false;

        AbstractCrawledResource that = (AbstractCrawledResource) o;

        return new EqualsBuilder()
                .append(resourceURI, that.resourceURI)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(resourceURI)
                .toHashCode();
    }
}
