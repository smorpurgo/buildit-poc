package org.morpurgo.buildit.crawler.spider.resource;

import java.net.URI;
import java.util.Set;

/**
 * Created by morpurgo on 13/09/16.
 */
public class WebPage
    extends AbstractCrawledResource
        implements CrawledResource {

    private final Set<String> internalLinks;
    private final Set<String> externalLinks;
    private final Set<ImageResource> images;

    public WebPage(URI uri,
                   Set<String> internalLinks,
                   Set<String> externalLinks,
                   Set<ImageResource> images,
                   URI parentURI) {
        super(uri, parentURI);
        this.internalLinks = internalLinks;
        this.externalLinks = externalLinks;
        this.images = images;
    }

    public Set<String> getInternalLinks() {
        return internalLinks;
    }

    public Set<String> getExternalLinks() {
        return externalLinks;
    }

    public Set<ImageResource> getImages() {
        return images;
    }

}
