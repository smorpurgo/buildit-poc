package org.morpurgo.buildit.crawler.spider.resource;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by morpurgo on 13/09/16.
 */
public class ImageResource extends AbstractCrawledResource
        implements CrawledResource {

    public ImageResource(URI uri, URI parent) {
        super(uri, parent);
    }

    public ImageResource(String url, String parentUrl) throws URISyntaxException {
        super(url, parentUrl);
    }
}
