package org.morpurgo.buildit.crawler.spider.resource;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * Created by morpurgo on 14/09/16.
 */
public class CandidateResource {

    private String url;
    private String parentUrl;

    public CandidateResource(String url, String parentUrl) {
        this.url = url;
        this.parentUrl = parentUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParentUrl() {
        return parentUrl;
    }

    public void setParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof CandidateResource)) return false;

        CandidateResource that = (CandidateResource) o;

        return new EqualsBuilder()
                .append(url, that.url)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(url)
                .toHashCode();
    }
}
