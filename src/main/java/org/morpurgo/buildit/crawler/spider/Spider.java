package org.morpurgo.buildit.crawler.spider;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.morpurgo.buildit.crawler.exception.CrawlException;
import org.morpurgo.buildit.crawler.exception.InvalidUrlException;
import org.morpurgo.buildit.crawler.options.SimpleUrlOptionParser;
import org.morpurgo.buildit.crawler.options.SpiderOptions;
import org.morpurgo.buildit.crawler.spider.report.ReportGenerator;
import org.morpurgo.buildit.crawler.spider.resource.CandidateResource;
import org.morpurgo.buildit.crawler.spider.resource.WebPage;
import org.morpurgo.buildit.crawler.spider.visitor.UrlVisitor;
import org.morpurgo.buildit.crawler.utils.UrlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by morpurgo on 13/09/16.
 */
@Component
public class Spider {

    @Autowired
    private SimpleUrlOptionParser simpleUrlOptionParser;

    //avoid visiting same node two times
    private Set<CandidateResource>     visited = new HashSet<>();

    // adding at the end of this list, the pages to visit,
    //the browsing will be breadth-first
    private List<CandidateResource>    toVisit = new LinkedList<>();

    private ResourceNode<WebPage> crawledPages;

    private Logger logger   =   Logger.getLogger(Spider.class);

    @Autowired
    private ReportGenerator reportGenerator;

    public void start(String[] args) throws CrawlException {

        SpiderOptions options;
        try {
            options = simpleUrlOptionParser.parse(args);
        } catch (InvalidUrlException e) {
            throw new CrawlException("Error while crawling", e);
        }

        final String startingUrl = options.getUrlToCrawl();
        toVisit.add(new CandidateResource(StringUtils.trim(startingUrl), null));
        String mainDomain;
        try {
            mainDomain = UrlUtils.getDomain(startingUrl);
        } catch (URISyntaxException ue) {
            throw new CrawlException("Unable to parse domain for the starting URL: " + startingUrl, ue);
        }

        long start = System.currentTimeMillis();

        while (!toVisit.isEmpty() && visited.size() <= options.getMaxUrls()) {
            final CandidateResource nextCandidate = nextUrl();
            if (nextCandidate != null) {
                visitNext(mainDomain, nextCandidate);
            }
        }

        long end    =   System.currentTimeMillis() - start;
        logger.info("End crawling, crawled in " + end + " millis");
        String htmlReport = reportGenerator.generateHtmlReport(crawledPages);
        reportGenerator.writeToFile(htmlReport);

    }

    private void visitNext(String mainDomain,
                           CandidateResource nextCandidate) throws CrawlException {
        String candidateUrl = nextCandidate.getUrl();
        String parentUrl  = nextCandidate.getParentUrl();
        try {
            if (StringUtils.isNotBlank(candidateUrl)) {
                UrlVisitor visitor = new UrlVisitor(candidateUrl, mainDomain, parentUrl);
                visitor.visit();
                toVisit.addAll(toCandidateResources(visitor.getInternalLinks(), candidateUrl));

                try {
                    final ResourceNode<WebPage> resourceNode = new ResourceNode<>(
                            new WebPage(
                                    new URI(candidateUrl),
                                    visitor.getInternalLinks(),
                                    visitor.getExternalLinks(),
                                    visitor.getImages(),
                                    visitor.getParent() != null ? new URI(visitor.getParent()) : null)
                    );
                    if (crawledPages == null) {
                        crawledPages = resourceNode;
                    } else {
                        //add the resource as children of the parentUrl resource
                        //this doesn't completely respect the website structure
                        // circular links, as
                        //we're ignoring already visited pages
                        crawledPages.addNodeByParentUrl(resourceNode);
                    }
                } catch (URISyntaxException e) {
                    logger.error("Unable to save crawled page", e);
                }
            }
        } catch (Throwable t) {
            logger.warn("Error occurred while visiting: " + candidateUrl + " ignoring this url", t);
        } finally {
            visited.add(nextCandidate);
        }
    }

    private Collection<CandidateResource> toCandidateResources(Set<String> links, String parent) {
        return links.stream().map(link -> new CandidateResource(link, parent)).collect(Collectors.toCollection(LinkedList::new));
    }

    private CandidateResource nextUrl() {
        CandidateResource nextUrl = toVisit.remove(0);
        while (visited.contains(nextUrl)) {
            if (!toVisit.isEmpty()) {
                nextUrl = toVisit.remove(0);
            } else {
                nextUrl = null;
            }
        }
        return nextUrl;
    }

}
