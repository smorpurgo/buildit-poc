package org.morpurgo.buildit.crawler.spider.visitor;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.morpurgo.buildit.crawler.exception.CrawlException;
import org.morpurgo.buildit.crawler.spider.resource.ImageResource;
import org.morpurgo.buildit.crawler.utils.UrlUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by morpurgo on 13/09/16.
 */
public class UrlVisitor {

    private final String visitingUrl;
    private final String mainDomain;
    private Logger logger   = LogManager.getLogger(UrlVisitor.class);

    private Set<String> internalLinks =   new HashSet<>();
    private Set<String> externalLinks      =   new HashSet<>();
    private Set<ImageResource> images              =   new HashSet<>();
    private String parent;

    public UrlVisitor(String visitingUrl, String mainDomain, String parent) {
        this.visitingUrl = visitingUrl;
        this.mainDomain = mainDomain;
        this.parent = parent;
    }

    public void visit() throws CrawlException {

        logger.info("Visit " + visitingUrl);

        Connection connection   = Jsoup.connect(visitingUrl);
        Document htmlDocument;
        try {
            htmlDocument = connection.get();
        } catch (IOException e) {
            throw new CrawlException("Unable to connect to url: " + visitingUrl, e);
        }

        //get all links
        Elements links   = htmlDocument.select("a[href]");

        //add to links to visit, filtering out links of external domains
        generateLinks(mainDomain, links);

        // this is ignoring all CSS resources
        Elements mediaResources = htmlDocument.select("[src]");

        generateMediaResources(mediaResources);

        logger.debug("Found " + links.size() + " links and " + mediaResources.size() + " resources");

    }

    private void generateMediaResources(Elements mediaResources) {
        for (Element src : mediaResources) {
            final String resourceUrl = src.attr("abs:src");
            if (src.tagName().equals("img")) {
                logger.debug("Found image:" + resourceUrl);
                try {
                    images.add(new ImageResource(resourceUrl, visitingUrl));
                } catch (URISyntaxException e) {
                    logger.warn("Unable to add image: " + resourceUrl);
                }
            } else {
                logger.debug("Found src " + src.tagName() + ": " + resourceUrl);
            }
        }
    }

    private void generateLinks(String mainDomain, Elements links) {
        for (Element link : links) {
            String absHref = link.attr("abs:href");
            absHref = UrlUtils.removeFragment(absHref);
            final String linkDomain;
            try {
                if (StringUtils.isNotBlank(absHref)) {
                    linkDomain = UrlUtils.getDomain(absHref);
                    if (StringUtils.isNotBlank(linkDomain)) {
                        if (linkDomain.equalsIgnoreCase(mainDomain)) {
                            //same domain: new link to visit
                            internalLinks.add(StringUtils.trim(absHref));
                        } else {
                            //external links
                            externalLinks.add(StringUtils.trim(absHref));
                        }
                    }
                }
            } catch (URISyntaxException e) {
                logger.warn("Unable to evaluate domain, do not add: "
                        + absHref + " to urls to visit");
            }
        }
    }

    public Set<String> getInternalLinks() {
        return internalLinks;
    }

    public Set<String> getExternalLinks() {
        return externalLinks;
    }

    public Set<ImageResource> getImages() {
        return images;
    }

    public String getParent() {
        return parent;
    }
}
