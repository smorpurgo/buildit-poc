package org.morpurgo.buildit.crawler;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by morpurgo on 13/09/16.
 */
@Configuration
@ComponentScan
public class CrawlerConfig {

}
