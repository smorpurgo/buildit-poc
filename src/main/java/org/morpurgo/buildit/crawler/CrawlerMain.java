package org.morpurgo.buildit.crawler;

import org.morpurgo.buildit.crawler.exception.CrawlException;
import org.morpurgo.buildit.crawler.spider.Spider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;

/**
 * Created by morpurgo on 13/09/16.
 */
public class CrawlerMain {

    public static void main(String[] args) throws CrawlException, InterruptedException, ExecutionException, IOException, URISyntaxException {
        final ApplicationContext context = new AnnotationConfigApplicationContext(CrawlerConfig.class);
        Spider spider = context.getBean("spider", Spider.class);
        spider.start(args);
    }

}
