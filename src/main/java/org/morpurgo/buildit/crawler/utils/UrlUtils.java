package org.morpurgo.buildit.crawler.utils;

import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by morpurgo on 13/09/16.
 */
public class UrlUtils {
    /**
     * the simplest implementation to extract domain from the url
     * something more complex can be implemented ( using regex / failovers, some common library )
     *
     * @param url url to get the domain from
     * @return the domain
     * @throws URISyntaxException if there's some syntax error in the url
     */
    public static String getDomain(String url) throws URISyntaxException {
        URI uri = new URI(url);
        String domain = uri.getHost();
        return removeWww(domain);
    }

    private static String removeWww(String domain) {
        return domain != null && domain.startsWith("www.") ? domain.substring(4) : domain;
    }

    public static String removeFragment(String absHref) {
        return StringUtils.isNotBlank(absHref) && StringUtils.contains(absHref, "#")
                ? StringUtils.split(absHref, '#')[0] : absHref;
    }
}
